using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eggshooter : MonoBehaviour
{
    public Rigidbody egg;

    void Start()
    {
        InvokeRepeating("LaunchEgg", 2.0f, 0.3f);
    }

    void LaunchEgg()
    {
        Rigidbody instance = Instantiate(egg);

        instance.velocity = Random.insideUnitSphere * 5;
    }
}
